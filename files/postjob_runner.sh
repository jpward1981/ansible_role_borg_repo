#!/usr/bin/env bash
#
### Program Name: borgbackup_run_jobs
#
### Program Author: DreamTeam 
#
### Program Version:
#
### Program Purpose:
# Borgbackup_run_jobs script is part of the borgbackup job that process notification and action requrests on behalf of the backup client. This is intented to be run by a cronjob 
#
################################################################################

# This section is for toggling debugging on/off
# "set -x" toggles debugging on
# "set +x" toggles debugging off
set +x;

borgbackup_script_path="$(readlink -f "${BASH_SOURCE[0]}" 2>/dev/null||echo $0)";
borgbackup_source_dir="$(dirname ${borgbackup_script_path})";
borgbackup_lib_dir="${borgbackup_source_dir%/bin}/lib/";
source "${borgbackup_lib_dir}/util-libs.sh";


for utilfunction in \
                    InputSanitize \
                    CheckIfEmpty \
                    utilDeps \
                    utilLog \
		    parse_yaml \
                    ;
do
    readonly -f "${utilfunction}";
done;

# check for dependencies
dep_pkgs=( awk sed git ansible )

utilDeps dep_pkgs

utilLog /var/log/borgbackup \
  || { \
        echo "failed"
        exit 1;
};

#Load yaml configuration file from /etc/borgbackup_server and parse values;
LoadYamlConfig(){
if [ -f "/etc/borgbackup/postjob_runner_config.yml" ]; then
#read yaml file
eval "$(parse_yaml /etc/borgbackup/postjob_runner_config.yml "config_")";

#set variables from values in config file
readonly username="$config_connection_username";
readonly ssh_key_file="$config_connection_ssh_key_file";
readonly borg_job_queue_path="$config_server_job_queue_path"
readonly borg_log_path="$config_server_log_dir"
readonly borg_home_dir="$config_server_home_dir"
readonly ansible_working_directory="$config_server_ansible_working_directory"

#test required server configuration variables
InputSanitize $username
#InputSanitize $ssh_key_file
#InputSanitize $borg_home_dir



else
   exit
fi
}


Downloader(){
     
     downloader=$(echo $1 | cut -f1 -d+)
     reposrc=$(echo $1 | cut -f2 -d+)
     echo "downloading files from $reposrc" > >(tee -a ${_r_log_dir}/${_r_datestamp}_backup_server_${_r_run_id}.event.log >&1);
     dest=$2
     case $downloader in 
       git)
         cd $dest && pwd && git clone $reposrc 2>&1 |  tee | LogInfo
       ;;
       wget)
         cd $dest && wget $reposrc 2>&1 |  tee | LogInfo
       ;; 
     esac
}

RunPostJob(){
#ensure playbook environment is clean
#cleanup: remove files and subdirectories if temporary directory is not empty
if [ "$(ls -A $tmp_dir)" ]; then
  for download_tmp_files in $(ls $tmp_dir); do 
    cd $tmp_dir && \
    rm -rf  $download_tmp_files
  done
fi 
# clean up remove files and subdirectories if playbook director is not empty 
if [ "$(ls -A $playbook_dir)" ]; then
  for playbook_files in $(ls $playbook_dir); do 
    cd $playbook_dir && \
    rm -rf  $playbook_files
  done;
fi 

# ensure playbook and role directories exist
#create temporary directory
mkdir -p $tmp_dir
# create the playbook directory 
mkdir -p $playbook_dir
# create the role directory for playbook
mkdir -p $ansible_role_path
Downloader $playbook_src $tmp_dir

playbook_repo_basename=$(basename $playbook_src)

for files in group_vars inventory *.yml; do
  mv $tmp_dir/${playbook_repo_basename%.*}/$files $playbook_dir/
done;

#download roles
ls -la /home/borg/playbook/roles
for cmd in "${jobcmd_role_requirements[@]}"; do
   Downloader $cmd $ansible_role_path
done;

# run playbook
echo "post run job defined. performing post run job" > >(tee -a ${_r_log_dir}/${_r_datestamp}_backup_server_${_r_run_id}.event.log >&1);
ansible-playbook -i "$playbook_dir/inventory/$(ls $playbook_dir/inventory)" --become --user=$ansible_user --private-key=$ssh_key_file  $playbook_dir/playbook.yml \
 &> >(tee -a ${_r_log_dir}/${_r_datestamp}_backup_server_${_r_run_id}.event.log >&1);
}

ProcessJob(){
echo "checking job queue for requests"  > >(tee -a ${_r_log_dir}/${_r_datestamp}_backup_server_${_r_run_id}.event.log >&1);
if [ "$(ls -A $borg_job_queue_path)" ]; then
  echo "job request found. processing..."  > >(tee -a ${_r_log_dir}/${_r_datestamp}_backup_server_${_r_run_id}.event.log >&1);
  for job in $(ls $borg_job_queue_path); do
  #read yaml file

  #read in the job yaml file to set environment variables
  eval "$(parse_yaml $borg_job_queue_path/$job "job_")";

  #set variables from values in config file
  backup_name="$job_name";
  export BACKUP="$backup_name"
  # re-read the job yaml file to run the user defined commands
  eval "$(parse_yaml $borg_job_queue_path/$job "jobcmd_")";
  executor="$jobcmd_post_run_executor";
  ansible_user=$jobcmd_ansible_user;
  playbook_src=$jobcmd_playbook;
  tmp_dir=$ansible_working_directory/download_tmp
  playbook_dir=$ansible_working_directory
  ansible_role_path=$ansible_working_directory/roles
  #echo $playbook_src
  InputSanitize $backup_name
  InputSanitize $executor 
  InputSanitize $ansible_user

  echo "running user defined post job" > >(tee -a ${_r_log_dir}/${_r_datestamp}_backup_server_${_r_run_id}.event.log >&1);
  RunPostJob;
  jobcmd_role_requirements=() 
  rm -f $borg_job_queue_path/$job
  done
fi
}

main(){
LoadYamlConfig
ProcessJob
}
main
